import api from "./api"
export const getTodoTasks=()=>{
    return api.get("/todo");
}
export const createTodoTask=(todoTask)=>{
    return api.post(`/todo`,todoTask)
}

export const deleteTodoTask = (id) => {
    return api.delete(`/todo/${id}`);
}

export const updateTodoTask = (id,todoTask) => {
    return api.put(`/todo/${id}`,todoTask);
}

export const getTodoById = (id) => {
    return api.get(`/todo/${id}`);
}


