import { useTodos } from "../../hooks/useTodo";
import { EditOutlined, DeleteOutlined, EyeOutlined } from "@ant-design/icons";
import { Modal, Input, Button, Form, Popover } from 'antd';
import { useState } from 'react';
import React from 'react';

export default function TodoItem({ task }) {

    const { updateTodo, deleteTodo, showTodoDetail } = useTodos();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const { TextArea } = Input;
    const [taskName, setTaskName] = useState("");
    const [taskId, setTaskId] = useState(task.id);

    const handleTaskNameClick = async () => {
        await updateTodo(task.id, { done: !task.done })
    }

    const hanlleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(task.id);
        }
    }
    const handleEditButtonClick = () => {
        setTaskName(task.name)
        setIsModalOpen(true);
    }

    const handleOk = async () => {
        await updateTodo(task.id, { name: taskName });
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setTaskName(task.name)
        setIsModalOpen(false);
        console.log("task.name=======", task.name)
        console.log("taskName=======", taskName)
    };

    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }

    const handleShowDetailButtonClick = () => {
        showTodoDetail(taskId);
    }
    const content = (
        <div>
            <p>{task.id}</p>
            <p>{task.name}</p>
        </div>
    );

    return (
        <div>
            <div className='todo-item'>
                <div className={`task-name ${task.done ? 'done' : ''}`}
                    onClick={handleTaskNameClick}>{task.name}</div>
                <div className='buttons'>
                    <div className='remove-button' onClick={hanlleRemoveButtonClick}><DeleteOutlined /></div>
                    <div className='show-detail-button' onClick={handleShowDetailButtonClick}>
                        <Popover content={content} title="Title" trigger="hover">
                            <EyeOutlined />
                        </Popover>
                    </div>
                    <div className='edit-button' onClick={handleEditButtonClick}><EditOutlined />v</div>
                </div>
            </div>
            <Modal title="Update todoItem" open={isModalOpen} onCancel={handleCancel} footer={[]} >
                <Form>
                    <TextArea value={taskName} onChange={handleTaskNameChange}></TextArea>
                    <Button type="primary" htmlType="submit" onClick={handleOk}>OK</Button>
                    <Button onClick={handleCancel}>Cancel</Button>
                </Form>
            </Modal>
        </div>
    );
}