import {useParams} from 'react-router-dom';
import {useSelector} from 'react-redux'
export default function DoneDetail(){
    const {id} = useParams();
    const todoTask = useSelector(state=>state.todo.tasks).find(task=>task.id==id)
    return(
        <div className="todo-detail">
            <h1>detail</h1>
            <div>{todoTask?.id}</div>
            <div>{todoTask?.name}</div>
        </div>
    )
}