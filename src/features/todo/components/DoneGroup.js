import {useSelector} from "react-redux";
import DoneItem from "./DoneItem";
export default function DoneGroup () {
    const doneTask = useSelector(state => state.todo.tasks).filter(task => task.done)
    return (
        <div className='todo-group'>
            {
              doneTask.map(((doneTask) =>
                <DoneItem key={doneTask.id} task={doneTask}></DoneItem>
              ))
            }
        </div>
    )
};
