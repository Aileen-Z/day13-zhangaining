import { useDispatch } from "react-redux";
import { removeTodoTask, updateTodoTaskStatus } from "../reducers/todoSlice";
import { useNavigate } from "react-router";
export default function DoneItem(props) {
    const dispatch = useDispatch();
    const navigation = useNavigate();
    const handlerTaskNameClick = () => {
            navigation('/done/' + props.task.id);
    }

    const hanlleRemoveButtonClick = () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            dispatch(removeTodoTask(props.task.id));
        }
    }
    return (
        <div className='todo-item'>
            <div className={`task-name ${props.task.done ? 'done' : ''}`}
                onClick={handlerTaskNameClick}>{props.task.name}</div>
            <div className='remove-button' onClick={hanlleRemoveButtonClick}>x</div>
        </div>
    );
}
