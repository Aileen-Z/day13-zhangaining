import * as todoApi from "../../apis/todo";
import {useDispatch} from "react-redux";
import {initTodoTasks} from "../todo/reducers/todoSlice";

export const useTodos=()=>{
    const dispatch = useDispatch();
    async function loadTodos(){
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTasks(response.data));
    }

    const updateTodo =async (id, todoTask)=>{
        await todoApi.updateTodoTask(id, todoTask)
        await loadTodos();
    }
    const deleteTodo =async(id)=>{
        await todoApi.deleteTodoTask(id);
        await loadTodos();
    }
    const createTodo =async(todoTask)=>{
        await todoApi.createTodoTask(todoTask);
        await loadTodos();
    }
    const showTodoDetail=async(id)=>{
        await todoApi.getTodoById(id);
        await loadTodos();
    }
    return {
        loadTodos,
        updateTodo,
        deleteTodo,
        createTodo,
        showTodoDetail,
    }
}